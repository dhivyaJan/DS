/**
 * Given an array words of strings made only from lowercase letters, return a list of all characters that show up in all strings within the list (including duplicates).  For example, if a character occurs 3 times in all strings but not 4 times, you need to include that character three times in the final answer.

You may return the answer in any order.

 

Example 1:

Input: ["bella","label","roller"]
Output: ["e","l","l"]
Example 2:

Input: ["cool","lock","cook"]
Output: ["c","o"]
 */

var commonChars = function(words) {
    let common = [];
   
   for(let i=0; i<words.length-1; i++){
       let curr = words[i];
       let next = words[i+1];
       let clone = [];
       for(let j=0; j< curr.length; j++){
           let foundIdx = next.indexOf(curr[j]);
           if(foundIdx >= 0 && i===0){
               common.push(curr[j]);
           }else if(foundIdx >= 0 && common.indexOf(curr[j]) >= 0){
              clone.push(curr[j]);
              common.splice(common.indexOf(curr[j]),1)
           }
           next = next.replace(next[foundIdx], '');
           if(j+1 === curr.length && (clone.length > 0 || i > 0)){
               common = clone.slice();
           }
       }
   }
   return common;
};

//TC: O(N^2)
//SC: O(N)