/**
 * Given a m x n matrix grid which is sorted in non-increasing order both row-wise and column-wise, return the number of negative numbers in grid.

 

Example 1:

Input: grid = [[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]]
Output: 8
Explanation: There are 8 negatives number in the matrix.
 */

/**
 * @param {number[][]} grid
 * @return {number}
 */
 var countNegatives = function(grid) {
    let count = 0;
    
    for(let i=0; i<grid.length; i++){
        for(let j=0; j<grid[i].length; j++){
            if(grid[i][j] < 0){
                count += grid[i].length - j;
                break;
            }
        }
    }
    return count;
};

//TC: O(N^2)
//SC: O(1)

    // Binary search
    // function countNegatives(grid) {
    //     let totalNegative = 0;
    //     for(let i=0;i<grid.length;i++){
    //         binarySearch(grid[i], totalNegative);
    //     }
    //     return totalNegative;
    // }
    
    // function binarySearch(nums, count){
    //     let len = nums.length-1;
    //     let left = 0, right = len;
    //     while(left<right){
    //         let mid = left+(right-left)/2;
    //         if(nums[mid]<0){
    //             right=mid;
    //         }else{
    //             left = mid+1;
    //         }
    //     }
    //     if(nums[left]<0){
    //         count+=len - left + 1;
    //     }
    // }