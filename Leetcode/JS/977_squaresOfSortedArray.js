/**
 * Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.

 

Example 1:

Input: nums = [-4,-1,0,3,10]
Output: [0,1,9,16,100]
Explanation: After squaring, the array becomes [16,1,0,9,100].
After sorting, it becomes [0,1,9,16,100].
Example 2:

Input: nums = [-7,-3,2,3,11]
Output: [4,9,9,49,121]
 */

var sortedSquares = function(nums) {
    let n = nums.length-1;
    let left = 0;
    let right = n;
    let result = [];
    
    for(let i=n; i>=0; i--){
        let square;
        if(Math.abs(nums[right]) > Math.abs(nums[left])){
            square = nums[right];
            right--;
        } else {
            square = nums[left];
            left++;
        }
        result[i] = square * square;
    }
    return result;
};

//TC: O(N)
//SC: O(N)