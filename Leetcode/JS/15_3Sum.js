/**
 * Given an integer array nums, return all the triplets [nums[i], nums[j], nums[k]] such that i != j, i != k, and j != k, and nums[i] + nums[j] + nums[k] == 0.

Notice that the solution set must not contain duplicate triplets.

 

Example 1:

Input: nums = [-1,0,1,2,-1,-4]
Output: [[-1,-1,2],[-1,0,1]]
 */

/**
 * @param {number[]} nums
 * @return {number[][]}
 */
 var threeSum = function(nums) {
    nums.sort((a, b) => a - b);
    let result = [];
    
    for(let i=0; i<nums.length && nums[i] <= 0; i++){
        if(i === 0 || (nums[i] != nums[i-1])){
            threeSumHelper(result, i, nums);
        }
    }
    return result;
};

function threeSumHelper(result, i, nums) {
    let low = i+1;
    let high = nums.length-1;
    
    while(low < high){
        let sum = nums[i] + nums[low] + nums[high];
        if(sum < 0){
            low++;
        }else if (sum > 0){
            high--;
        }else{
            result.push([nums[i], nums[low], nums[high]]);
            low++; high--;
            while(low < high && nums[low] === nums[low-1]){
                low++;
            }
        }
    }
}

//TC: O(N^2)
//SC: O(N) or O(NlogN)
