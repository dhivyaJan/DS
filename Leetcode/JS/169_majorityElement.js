/**
 * 
 * Given an array nums of size n, return the majority element.

The majority element is the element that appears more than ⌊n / 2⌋ times. You may assume that the majority element always exists in the array.

 

Example 1:

Input: nums = [3,2,3]
Output: 3
Example 2:

Input: nums = [2,2,1,1,1,2,2]
Output: 2
 */

/**
 * @param {number[]} nums
 * @return {number}
 */
 var majorityElement = function(nums) {
    let map = {};
    let n = nums.length;
    for(let i=0; i<nums.length; i++){
        map[nums[i]] = map[nums[i]] ? map[nums[i]] + 1 : 1;
        // if(map[nums[i]] > Math.floor(n/2)){
        //     return nums[i];
        // }
    }
    
    for(let i=0; i<nums.length; i++){
        if(map[nums[i]] > Math.floor(n/2)){
            return nums[i];
        }
    }
    return -1;
};

//TC: O(N)
//SC: O(N)

// function majorityElement(nums) {
//     nums.sort();
//     return nums[nums.length/2];
// }

//TC: O(NlogN)
//SC: O(1)

//Boyer-Moore Voting Algorithm
// var majorityElement = function(nums) {
//     let majority = null;
//     let count = 0;
     
//     for(let num of nums){
//         if(count === 0){
//             majority = num;
//         }
        
//         count += (num === majority) ? 1 : -1;
//     }
//      return majority;
//  };

 //TC: O(N)
 //SC: O(1)
