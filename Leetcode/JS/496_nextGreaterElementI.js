/**
 * The next greater element of some element x in an array is the first greater element that is to the right of x in the same array.

You are given two distinct 0-indexed integer arrays nums1 and nums2, where nums1 is a subset of nums2.

For each 0 <= i < nums1.length, find the index j such that nums1[i] == nums2[j] and determine the next greater element of nums2[j] in nums2. If there is no next greater element, then the answer for this query is -1.

Return an array ans of length nums1.length such that ans[i] is the next greater element as described above.

 

Example 1:

Input: nums1 = [4,1,2], nums2 = [1,3,4,2]
Output: [-1,3,-1]
Explanation: The next greater element for each value of nums1 is as follows:
- 4 is underlined in nums2 = [1,3,4,2]. There is no next greater element, so the answer is -1.
- 1 is underlined in nums2 = [1,3,4,2]. The next greater element is 3.
- 2 is underlined in nums2 = [1,3,4,2]. There is no next greater element, so the answer is -1.

 */

var nextGreaterElement = function(nums1, nums2) {
    let result = [];
    for(let i=0; i<nums1.length; i++){
        let sameNumPresent = false;
        result[i] = -1;
        for(let j=0; j<nums2.length; j++){
            if(nums2[j] === nums1[i]){
                sameNumPresent = true;
            }
            if(nums2[j] > nums1[i] && sameNumPresent){
                result[i] = nums2[j];
                break;
            }
        }
    }
    return result;
};

//TC: O(N^2)
//SC: O(N)


// var nextGreaterElement = function(nums1, nums2) {
//     let map = {};
//     let stack = [];
     
//     for(let i=0; i<nums2.length; i++){
//         while(stack.length && stack[stack.length-1] < nums2[i]){
//             map[stack.pop()] = nums2[i]; 
//         }
//         stack.push(nums2[i]);
//     }
     
//     for(let i=0; i<nums1.length; i++){
//         nums1[i] = map[nums1[i]] || -1;
//     }
//     return nums1;
//  };

//TC: O(N) stack push, pop operations happen once
//SC: O(N) hash mapstores n key-value pairs