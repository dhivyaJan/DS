/**
 * Given an integer n, return any array containing n unique integers such that they add up to 0.

 

Example 1:

Input: n = 5
Output: [-7,-1,1,3,4]
Explanation: These arrays also are accepted [-5,-1,1,2,3] , [-3,-1,2,-2,4].
Example 2:

Input: n = 3
Output: [-1,0,1]
Example 3:

Input: n = 1
Output: [0]
 */

/**
 * @param {number} n
 * @return {number[]}
 */
 var sumZero = function(n) {
    let result = [];
    let idx = 0; let value = 1;
    while(n > 1){
        result[idx++] = value;
        result[idx++] = -1*value;
        
        n=n-2;
        value++;
    }
    
    if(n > 0){
        result[idx] = 0;
    }
    return result;
};

//TC: O(N)
//SC: O(N)