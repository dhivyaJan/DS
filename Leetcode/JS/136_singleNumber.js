/**
 * 136. Single Number
Easy

6855

225

Add to List

Share
Given a non-empty array of integers nums, every element appears twice except for one. Find that single one.

You must implement a solution with a linear runtime complexity and use only constant extra space.
Input: nums = [2,2,1]
Output: 1
Example 2:

Input: nums = [1]
Output: 1
 */

var singleNumber = function(nums) {
    nums.sort((a,b) => a-b);
    let count = 1;
    
    for(let i=1; i<nums.length; i++){
        if(nums[i] !== nums[i-1]){
            if(count === 0){
                count = 1;
            } else {
                return nums[i-1];
            }
        } else {
            if(count === 1){
                count = 0;
            } else {
                count++; 
            }
        }
    }
    if(count === 1){
        return nums[nums.length-1];
    }
    return 0;
};

//TC: O(N log N)
//SC: O(1)

/// bit manipulation 

    // function singleNumber(nums) {
    //   let a = 0;
    //   for (let i of nums) {
    //     a ^= i;
    //   }
    //   return a;
    // }

//TC: O(N)
//SC: O(1)