/**
 * 
 *Given the array nums, for each nums[i] find out how many numbers in the array are smaller than it. That is, for each nums[i] you have to count the number of valid j's such that j != i and nums[j] < nums[i].

Return the answer in an array.

 

Example 1:

Input: nums = [8,1,2,2,3]
Output: [4,0,1,1,3]
Explanation: 
For nums[0]=8 there exist four smaller numbers than it (1, 2, 2 and 3). 
For nums[1]=1 does not exist any smaller number than it.
For nums[2]=2 there exist one smaller number than it (1). 
For nums[3]=2 there exist one smaller number than it (1). 
For nums[4]=3 there exist three smaller numbers than it (1, 2 and 2). 
 */

/**
 * @param {number[]} nums
 * @return {number[]}
 */
 var smallerNumbersThanCurrent = function(nums) {
    let result = [];
    
    for(let i=0; i<nums.length; i++){
        let count = 0;
        for(let j=0; j<nums.length; j++){
            if(i !== j){
                if(nums[j] < nums[i]){
                    count++;
                }
            }
        }
        result.push(count);
    }
    return result;
};

//TC: O(N^2)
//SC: O(N)

/**
 * var smallerNumbersThanCurrent = nums => {
    let sortedArr = nums.slice().sort((a, b) => a - b);
    return nums.map(num => sortedArr.indexOf(num));
};

TC: O(NlogN)
SC: O(N)
 */

/**
 * 
 * function smallerNumThanCurr(nums){
 *  let count = [];//of length 101
 * //let count = Array.from({length: 101}, ()=>0)
        
        for(let num of nums){
            count[num]++;
        }
        
        for(let i = 1; i< count.length; i++){
            count[i] += count[i-1];
        }
        // now count array is running sum array.
        
        let res = [];
		
        for(let i = 0; i< nums.length; i++){
            if(nums[i] == 0)
                res[i] = 0;
            else
                res[i] = count[nums[i]-1];
        }
		
        return res;

    TC: O(N)
    SC: O(N)
 * }
 */